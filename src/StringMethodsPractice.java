import java.util.Arrays;

public class StringMethodsPractice {
    public static void main(String[] args) {
//        Copy the following code into your main method.
//        String myLength = "Good afternoon, good evening, and good night";
//        Write some java code that will display the length of the string
//        System.out.println(myLength.length());
//        Write some java code that will display the entire string in all uppercase
//        System.out.println(myLength.toUpperCase());
//        Use the same code but instead of using the toUpperCase() method, use the toLowerCase() method.
//        System.out.println(myLength.toLowerCase());
//                Print it out in the console, what do you see?
//        Copy this code into your main method
//        String firstSubstring = "Hello World".substring(10);
//        Print it out in the console, what do you see?
//        World
//        System.out.println(firstSubstring);
//        Change the argument to 3, print out in the console. What do you get?
//        lo World
//        Change the argument to 10, print it out in the console. What do you get?
//          d
//        Copy this code into your main method
//        String message = "Good evening, how are you?".substring(0, 12);
//        System.out.println(message);
//        Using the substring() method, make your variable print out "Good evening"
//        Now using the substring() method, try to make your variable print out "how are you?"
//        String message = "Good evening, how are you?";
//        System.out.println(message.substring(14));

//        Create a char variable named 'myChar' and assign the value "San Antonio"
//        String myChar = "San Antonio";
//        char result = myChar.charAt(0);
//        System.out.println(result);
//        Using the charAt() method return the capital 'S' only.
//                Change the argument in the charAt() method to where it returns the capital 'A' only.
//        char result = myChar.charAt(4);
//        System.out.println(result);

//                Change the argument in the charAt() method to where it returns the FIRST lowercase 'o' only.
//        char result = myChar.charAt(7);
//        System.out.println(result);

//                Create a String variable name 'bravo' and assign all the names in the cohort in ONE string.
//                Using the split() method, create another variable named 'splitBravo' where it displays the list of names separated by a comma.
        String bravo = " MaryAnn Sandra Jonathan Eric Adrian Henry";
        String[] splitBravo = bravo.split(" ");
        System.out.println(Arrays.toString(splitBravo));

//                Print out your results in the console
//        Ex. [Peter, John, Andy, David];
//        Use the following string variables.
        String m1 = "Hello,";
        String m2 = "how are you?";
        String m3 = "I love Java!";
//        Using concatenation, print out all three string variables that makes a complete sentence.
//        System.out.format(m1 + " " + m2 + " " + m3);
//        Use the following integer variable
        int result = 89;
//        Using concatenation, print out "You scored 89 marks for your test!"
//                *89 should not be typed in the sout, it should be the integer variable name.
        System.out.format("You scored " + result + " marks for your test!");
    }
}
