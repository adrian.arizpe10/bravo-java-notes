package Inherit1;

public class Animal {
    public void displayInfo(){
        System.out.println("I am an animal");
    }
}
class Dog extends Animal{
    @Override
    public void displayInfo() {
        System.out.println("I am a dog.");
    }
} // end of class
class Cat extends Animal{
    @Override
    public void displayInfo() {
        System.out.println("I am a cat meow!");
    }
}

//    protected String type;
//    private String color;
//    private int age;
//
//    public void eat(){
//        System.out.println("i am eating");
//    }
//
//    public void sleep(){
//        System.out.println("I am sleeping");
//    }
//    public String getColor() {
//        return color;
//    }
//
//    public void setColor(String col) {
//        color = col;
//    }
//    public String getType() {
//        return type;
//    }
//
//    public void setType(String type) {
//        this.type = type;
//    }
//    public int getAge() {
//        return age;
//    }
//
//    public void setAge(int age) {
//        this.age = age;
//    }
//
//
//
//
//    public static void main(String[] args) {
//        Dog dog = new Dog();
//        dog.eat();
//
////        Cat cat = new Cat();
////        cat.sleep();
//    }
//}
//class Dog extends Animal{
//    @Override
//    public void eat() {
//        super.eat();
//        System.out.println("I am eating dog food!");
//    }
//
//    public void displayInfo(String c, String t, int a){
//        System.out.println("I am a " + t);
//        System.out.println("My color is " + c);
//        System.out.println("I am the age of: " + a);
//    }
////    String breed;
//    public void bark(){}
//}
////class Cat extends Animal{
////    int age;
////    public void meow(){}
////}
