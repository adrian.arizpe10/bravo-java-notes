package Inherit1;

public class Main {
    public static void main(String[] args) {
        //declaration of the object variable a1 of the animal class
        //example of polymorphism
        Animal a1;
        //Object creation of the animal class
        a1 = new Animal();
        a1.displayInfo();
        //object creation of the dog class
        a1 = new Dog();
        a1.displayInfo();
        //object creation of cat class
        a1 = new Cat();
        a1.displayInfo();
    }

}
//    public static void main(String[] args) {
//        Dog dog1 = new Dog();
//        dog1.eat();
//        dog1.displayInfo("red", "Labrador", 2);
//    }
//}
