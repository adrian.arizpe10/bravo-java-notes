package Shapes;

import Shapes.Circle;

public class TestCircle {
    public static void main(String[] args) {
        /**
         *  A Test Driver for the Shapes.Circle class
         */
/**Save as "Shapes.TestCircle.java"
 // Declare an instance of Shapes.Circle class called c1.

 // Construct the instance c1 by invoking the "default" constructor
 // which sets its radius and color to their default value.
 // Invoke public methods on instance c1, via dot operator.
 //The circle has radius of 1.0 and area of 3.141592653589793
 // Declare an instance of class circle called c2.
 // Construct the instance c2 by invoking the second constructor
 // with the given radius and default color.
 // Invoke public methods on instance c2, via dot operator.
 //The circle has radius of 2.0 and area of 12.566370614359172
 /** Now, run the Shapes.TestCircle and study the results.*/
// Shapes.Circle 1
        Circle c1 = new Circle();
        System.out.println("Radius: " + c1.getRadius() + " \n" + "Area: " + c1.getArea());
// Shapes.Circle 2
        Circle c2 = new Circle(2.0);
        System.out.println("Radius: " + c2.getRadius() + " \n" + "Area: " + c2.getArea());

    } // End Of Main
} // End of Class
