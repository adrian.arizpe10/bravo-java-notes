package Shapes;

public class Circle {
    // instance variables - private - not available outside of the class
    private double radius;
    private String color;
    public Circle(){ // first default constructor
        radius = 1.0;
        color = "red";
    }
    public Circle(double radius, String color) {
        this.radius = radius;
        this.color = color;
    }
    public Circle(double r) {  // 2nd constructor
        radius = r;
    }
    public double getRadius(){
        return radius;
    }
    public double getArea(){
        return radius * radius * Math.PI;
    }
    public String getColor(){
        return color;
    }
}
