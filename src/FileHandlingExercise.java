import java.io.*;

public class FileHandlingExercise {
    public static void main(String[] args) {

        String album1 = "Album: Suicide Season \n=======\nArtist: Bring Me the Horizon\nYear: 2008\nGenre: Metal";
        String album2 = "Album: After Hours \n=======\nArtist: The Weeknd\nYear: 2020\nGenre: Pop";
        String album3 = "Album: I Brought You My Bullets, You Brought Me Your Love \n=======\nArtist: My Chemical Romance\nYear: 2002\nGenre: Metal";
        try(BufferedWriter writer = new BufferedWriter(new FileWriter("./albums/albums.txt", true)))
        {
            writer.write(album1);
            writer.newLine();

            writer.write(album2);
            writer.newLine();

            writer.write(album3);
            writer.newLine();
        }
        catch (IOException e){
            System.out.println(e.getMessage());
        }
        String line;

        BufferedReader bufferedReader = null;

        try {
            bufferedReader = new BufferedReader(new FileReader("./albums/albums.txt"));
            line = bufferedReader.readLine();
            while (line != null){
                System.out.println(line);
                line = bufferedReader.readLine();
            }
        }
        catch (IOException e){
            System.out.println(e.getMessage());
        }
        finally {
            try {
                if (bufferedReader != null){
                    bufferedReader.close();
                }
            }
            catch (IOException e){
                System.out.println(e.getMessage());
            }
        }
        File albums = new File("./albums/albums.txt");
        albums.delete();
    } // end of main
} // end of class
