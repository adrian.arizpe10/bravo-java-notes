import JavaAbs.Example1;

interface Inter { // interface 2
    public void display1(); // abstract method to be implemented on object creation
}
//Second Interface
interface Example2{
    public void display2();
}
//This interface is extending both the above interfaces
interface Example3 extends Inter, Example2{ //This interface is using inheritance
}
//implementing interface called example 3 using the keyword implements
class Example4 implements Example3{ // this class is implementing example 3 because Example 3 inherits Interface then it must implement interface methods
    @Override
    public void display1() {
        System.out.println("Display 2 method");
    }

    @Override
    public void display2() {
        System.out.println("Display 3 method");
    }
}
class Demo{
    public static void main(String[] args) {
        Example4 obj = new Example4();
        obj.display1();
    }
}
