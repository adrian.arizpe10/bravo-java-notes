public class MethodLesson {
    public static void main(String[] args) {
        //METHODS - are a sequence of statements that performs a specific task
        // Call my greeting method ()
//        greeting("Hello", "bravo");
//        System.out.println(returnFive());
//        System.out.println(yelling("Hello World"));
//        System.out.println(message("Newsletter ", "Replied"));
//        String s = "This is a string";
//
//        String changeMe = "Hello Bravo!";
//        String secondString = "Hello Codebound!";
//        changeString(secondString);
//        System.out.println(secondString);

        counting(13);

    } // End of Main
    // basic syntax for a method:
//    public static returnType methodName(param1, param2,) {
//        // we want the code to do
//    }

//    public static String greeting(String name) {
//        return String.format("Hello %s!", name);
//    }
// void keyword means it doesnt expect a return statement
//    public static void greeting(String greet, String name) {
//        System.out.printf( "%s, %s!\n",greet, name );
//    }

//    public static int returnFive(){
//        return 5;
//    }
//
//    public static String yelling(String s) {
//        return s.toUpperCase();
//    }

    // public - this is the visibility modifier
    // defines whether or not other classes can "see" this method

    //static - the presence of this keyword defines that the method belongs to the class, as opposed to instances of it.

    // Method Overloading
    // - defining multiple methods with the same name, but with different parameter types, parameter order, or number of parameters

//    public static String message(){
//        return "This is an example of methods";
//    }
//    public static String message(String memo){
//        return memo;
//    }
//    public static String message(int code){
//        return "Code: " + code + " message";
//    }
//    public static String message(String memo, String status){
//        return memo + "Status: " + status;
//    }

    // Passing parameters to methods

//    public static void changeString(String s){
//    }

    // Recursion - that aims to solve a problem by dividing into small chunks

    // Counting 5 to 1 using recursion
    public static void counting(int num){
        if (num <= 0) {
            System.out.println("All done");
            return;
        }
        System.out.println(num);
        counting(num - 1);
    }


} // End of Class
