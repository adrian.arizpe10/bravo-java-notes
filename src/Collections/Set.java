package Collections;
//Set is a child interface of collections
//If we want to represent a group of individual objects as a single entity where duplicates are not allowed and insertion order is not preserved we would want to go for set

import java.util.*;

//A java set does not make any promises about the order of the elements kept internally
//Properties of set
//Contain only unique elements (not common and no duplicates, no two elements are the same)
//Order of elements in a set is implementation dependent
// Hashset elements are order on has code of elements
//This interface models the mathematical set abstraction and is used to represent sets, (ex. deck of card is a set)
//The java collection platform contains three general Set implementations: HashSet, TreeSet and LinkedHashSet you can use iterator of foreach loop to traverse the elements of a set.
public class Set {
    public static void main(String[] args){
    //HashSet class internally uses HashMap to store the objects.
    //The elements you enter into HashSet will be stored as Keys of HashMap and their values will be constant
    //HashSet does not allow duplicates, if you insert a duplicate, the older element will be overwritten

    //Creating a HashSet Object
    HashSet <String> set = new HashSet<String>();
    //Here we have created a HashSet called set

    HashSet<Integer> numbers = new HashSet<>();

        numbers.add(1);
        numbers.add(2);
        numbers.add(3);
//        System.out.println("Numbers HashSet " + numbers);

        //calling iterator method
        Iterator<Integer> iterate = numbers.iterator();
//        System.out.println("HashSet using iterator: " );
        //accessing elements below
        while (iterate.hasNext()) {
            System.out.println(iterate.next());
            //Using remove() method
//            boolean value1 = numbers.remove(2);
//            System.out.println("Is 2 removed " + value1);
//            System.out.println(numbers);
        }

        //Example using Tree Set
        TreeSet<Integer> trees = new TreeSet<Integer>();

        //Adding elements to the tree set
        trees.add(20);
        trees.add(30);
        trees.add(40);
        trees.add(50);
        trees.add(60);
        trees.add(70);

//            System.out.println("Your TreeSet is: " + trees);

        //Using a higher method-returns the lowest element among the elements greater than the specified element.
//        System.out.println("Using higher: " + trees.higher(40));

        //Using the lower-return the greatest element among those elements that are less than the specified elements\
//        System.out.println("Using lower: " + trees.lower(60));

        // Using the ceiling - returns the lowest element amongst those that are greater than the specified element. If the element passed exists in a treeSet, it returns the element passed as an argument
        System.out.println("Using ceiling " + trees.ceiling(10));

        //Using floor-returns the greatest element among those elements that are less than the specified element. If the element passed exists in the tree set, it returns the element passed as an argument
        System.out.println("Using floor " + trees.floor(30));

        //LinkedHashSet - is an ordered version of the HashSet that maintains a doubly-linked list across all elements. Use this class instead of HashSet when you care about iteration order. When you iterate through a HashSet the order is unpredictable, while a LinkedHashSet lets you iterate through the elements in the order in which they were inserted.
        //LinkedHashSet constructors
//        new LinkedHashSet(); // default constructor
//        Object c = null;
//        new LinkedHashSet(Collection ); // creates a LinkedHashSet from Collection c

        LinkedHashSet<String> linkedSet = new LinkedHashSet<String>();
        linkedSet.add("BMW");
        linkedSet.add("Honda");
        linkedSet.add("Audi");
        linkedSet.add("VW");
        linkedSet.add("BMW");
        System.out.println(linkedSet);
        System.out.println("Size of linkedSet " + linkedSet.size());
        linkedSet.remove("BMW");
        System.out.println(linkedSet);
    }
}
