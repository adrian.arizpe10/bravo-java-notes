package Collections;

import java.util.ArrayList;

//In this example we will be looking at the .set that is available to us in ArrayList
public class setExample {
    public static void main(String[] args) {
        ArrayList<Integer> arrayList = new ArrayList<Integer>();
        arrayList.add(1);
        arrayList.add(2);
        arrayList.add(3);
        arrayList.add(4);
        arrayList.add(5);
        arrayList.add(6);
        arrayList.add(7);
        System.out.println("ArrayList before update: " + arrayList);

        //Updating the first element
        arrayList.set(0, 11);
        System.out.println(arrayList);
        arrayList.set(1,22);
        System.out.println(arrayList);

    }
}
