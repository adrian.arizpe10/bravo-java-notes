package Collections;

import java.util.ArrayList;
import java.util.Collections;

class Student{
    int roll;
    String name;
}
public class collectionsLecture {

    /*Collections and there are two main types of collections used in Java these are:
    Array List
    Hashmap
    Is a data structure that can be used to group or collect, objects
     */

    //Array List
    //Represents an array that can be changed in size. All elements in an array must be of the same object and data type
    /*.size() returns the number of elements in the array
    .add() adds an element to the collection at the specified index
    .get() returns the element specified at the index
    .indexOf returns the first found index of the given item. If given item not found, returns -1
    .contain() checks to see if the arrayList contains the given element
    .lastIndexOf()  finds the last index of the given element
    .isEmpty() checks to see if the list is empty
    .remove() removes the first occurrence fof an item or an item at given index
     */

    //Examples

    public static void main(String[] args) {
//        ArrayList<String> list1 = new ArrayList<String>(); // declared an array list and defined using the key word of new give datatype
//        list1.add("Jane");
//        list1.add("Jim");
//        list1.add("Jill");
//        list1.add("Jack");
//        list1.remove("Jim");
//        System.out.println("list of list1 is: " + list1);
//
//        Student s1 = new Student();
//        s1.roll = 23455;
//        s1.name = "June";
//        ArrayList list2 = new ArrayList();
//        list2.add("Jim");
//        list2.add(10);
//        list2.add(s1);
//        System.out.println("list of list 2 is: " + list2);
//
//        ArrayList <Integer> numbers1 = new ArrayList<Integer>();
//        numbers1.add(10);
//        numbers1.addAll(list2);
//        System.out.println(numbers1);
//
//        numbers1.add(1);
//        numbers1.add(8);
//        numbers1.add(4);
//        numbers1.add(2);
//        numbers1.add(0);
//        numbers1.add(6);
//        numbers1.add(7);
//        numbers1.add(8);
//        numbers1.add(9);
//        System.out.println(numbers1);
//        System.out.println(numbers1.get(2)); // getting element at index of 2
//        System.out.println(numbers1.set(4, 8));
//        System.out.println(numbers1);
//        System.out.println(numbers1.remove(0));//remove first index
//        System.out.println(numbers1);
//
//        Collections.sort(numbers1);
//        System.out.println(numbers1);
//        System.out.println(numbers1.size());

        ArrayList <String> roast = new ArrayList<String>();
        roast.add("Medium");
        roast.add("Dark");
        roast.add("Light");
        roast.add("raw");
        roast.add("Well done");
        System.out.println(roast);//prints out roast
        System.out.println(roast.contains("Well done")); // checking to see if ArrayList contains given value
        System.out.println(roast.lastIndexOf("Light")); // return index of given value
        System.out.println(roast.isEmpty()); // checking if ArrayList is empty
roast.clear();
        System.out.println(roast);
    }
}
