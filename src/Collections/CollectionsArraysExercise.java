package Collections;

import java.util.*;

/*TODO:
            Create a new class called CollectionsArraysExercise
            Create a program that will print out an array of integers.
            Create a program that will print out an array of strings
            Create a program that iterates through all elements in an array. The array should hold the names of everyone in Alpha Class
            Create a program to insert elements into the array list. This element should be added on the first element and last. You can use a previous array or create a new one.
            Create a program to remove the third element from an array list. You can create a new array or use a previous one.
            Create an array of Dog breeds. Create a program that will sort the array list.
            Create an array of cat breeds. Create a program that will search an element in the array List.
            Now create a program that will reverse the elements in the array.
 */
public class CollectionsArraysExercise {
    public static void main(String[] args) {
        ArrayList numbers = new ArrayList();
        numbers.add(1);
        numbers.add(2);
        numbers.add(3);
        numbers.add(4);
        numbers.add(5);
        numbers.add(6);
        numbers.add(7);
//        System.out.println(numbers);

        ArrayList bravo = new ArrayList();
        bravo.add("Henry");
        bravo.add("MaryAnn");
        bravo.add("Sandra");
        bravo.add("Jonathan");
        bravo.add("Eric");
        bravo.add("Adrian");
//        System.out.println(bravo);

        for (Integer i = 0; i < bravo.size(); i++){
            System.out.println(bravo.get(i));
        }

        LinkedList arrayList = new LinkedList();
        arrayList.add("Anthony");
        arrayList.add("Aiden");
        arrayList.add("Ariel");
        arrayList.add("Adrian");
        System.out.println(arrayList);

        arrayList.addFirst("Amber");
        arrayList.addLast("Aaron");
        System.out.println(arrayList);
        arrayList.remove(3);
        System.out.println(arrayList);

        ArrayList animals = new ArrayList<>(Arrays.asList(
                "Monkey",
                "Giraffe",
                "Elephant",
                "Lemur",
                "Peacock"
        ));
        animals.add(0, "Ferret");
        System.out.println(animals);
        animals.add(6, "Parrot");
        System.out.println(animals);

        ArrayList dogs = new ArrayList();
        dogs.add("Pit Bull");
        dogs.add("PLabrador");
        dogs.add("English Bulldog");
        dogs.add("Maltese");
        dogs.add("Great Dane");
        System.out.println(dogs);
        Collections.sort(dogs);
        System.out.println(dogs);

        ArrayList cats = new ArrayList();
        cats.add("Tiger");
        cats.add("Kitty");
        cats.add("Kitten");
        cats.add("Lion");
        cats.add("Cheetah");
        if (cats.contains("Cheetah")){
            System.out.println("cheetah");
        } else {
            System.out.println("No cheetah");
        }

    }// end of main

}//end of class
