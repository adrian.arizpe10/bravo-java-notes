package Collections;

import java.util.ArrayList;

public class Clone {
    public static void main(String[] args) {
        ArrayList<String> al = new ArrayList<String>();

        //adding elements to arraylist
        al.add("Apple");
        al.add("Orange");
        al.add("Mango");
        al.add("Grapes");

        System.out.println("ArrayList is " + al);
        ArrayList<String> al2 = (ArrayList<String>)al.clone();
        System.out.println("Shallow copy of ArrayList " + al2);

        //Add and remove on cloned arrayList
        al2.add("Fig");//adding fig
        al2.remove("Grapes");//removing grapes
        System.out.println(al);//printing out arrayList

        //Display Array list after add and remove
        System.out.println("Original arrayList: " + al);
        System.out.println("Cloned arrayList: " + al2);

    }
}
