package Collections;
/*List Interface - an ordered or sequential collection of objects
Has some methods which can be used to store and manipulate the ordered collection of objects
The classes which implement the list interface are called as lists
These class are:
ArrayList
Vector
LinkedList
We can also used the list interface since ArrayList is implemented from List
In list you have control over the location on where an element is inserted or removed
All 15 methods of collections util library are available in addition to:
E get (int index) - returns element at specified index
E set (int index, E element) - Replaces an element at the specified position with the passed element
void add (int index, E element) - Inserts passed element at a specified index
E remove (int index) - this removes an element at a specified index
Int index (object o) - It returns an index of first occurrence of passed object
int lastIndexOf (Object o) - returns an index of last occurrence of passed object
ListIterator<E>listIterator() - It returns a list iterator over the elements of this list
ListIterator<E>listIterator(int index) - it returns a list iterator over the elements of this list starting at the specified index
List<E> subList (int fromIndex, int toIndex) - returns sub list of this list starting from index to index
 */
//Examples
// ArrayList
// List
// Vector
// LinkedList
// Stack List

import java.util.*;

public class List {
    public static void main(String[] args) {


        //syntax for creating instance of a ArrayList
        ArrayList a = new ArrayList(); // Example 1 (most commonly used)

        //LinkedList example 2
        LinkedList b = new LinkedList();

        //Vector example 3
        Vector c = new Vector();

        //Stack List example 4
        Stack d = new Stack();

        //Here is how we create an ArrayList in Java
        //Here, Type indicates the type of an array list

        // 1) ArrayList<Type> arrayList = new ArrayList<>();
        //    ArrayList <Integer> numbers = new ArrayList<>();

        //Initialize an ArrayList using asList()
        //    new ArrayList<>(Arrays.asList("Cat", "Dog", "Cow"));

        //We can use ArrayList using the list interface
//    List<String> list = new ArrayList();

        //Example creating string type of arrayList
        ArrayList <String> words = new ArrayList<>();
        //calling on collections methods from java.util
        words.add("Dog");
        words.add("Cat");
        words.add("Cow");
        System.out.println("ArrayList: " + words);

        // We can also add using indexes
        words.add(3, "Mouse");
        words.add(4, "Sheep");
        words.add(5, "Zebra");
        System.out.println("ArrayList: " + words);

        // Create an Integer arrayList, integers it the corresponding wrapper class of int type
        ArrayList<Integer> nums = new ArrayList<>();
        nums.add(1);
        nums.add(2);
        nums.add(3);
        nums.add(4);

        System.out.println("Array List as an integer is: " + nums);

        LinkedList<String> list = new LinkedList<String>();

        //Adding elements to the linkedList
        list.add("Steve");
        list.add("Carl");
        list.add("Tim");
        System.out.println(list);

        //adding an element to the first position
        list.addFirst("Nyomi");
        System.out.println(list);

        // adding an element to the last position
        list.addLast("Jim");
//        System.out.println(list);

        //adding an element to third position using index
        list.add(2, "Glenn");
//        System.out.println(list);

        //using iterator interface
        Iterator<String> iterator = list.iterator();
        while (iterator.hasNext()){
            System.out.println(iterator.next());

            System.out.println(list);

        // In this example we will use the asList method to create ArrayList

//        ArrayList <String> groceries = new ArrayList<String>(Arrays
//        // Then use the asList method to convert array into ArrayList
//        asList("Milk", "Chips", "Bread"));
//        System.out.println("Array List" + groceries);

        //Access elements of the arrayList
//        String element = groceries.get(1);

    }

    //Linked list are linked together using pointers, each element of the linked list references to the next element of the linked list.
    // Each element of the linked list is called a node.
    //Each node of the linked list contains two items
    //1) Content of the element
    //2)Pointers/address/reference to the next node in the list
    //Insert and delete operations in the linked list are not performance wise expensive because adding and deleting an element from the linked list doesn't require the element to shift

    //In the following example we are using add(), addFirst(), and addLast() methods to add elements to the desired locations in the LinkedList
//    public void linkedList(){
//        LinkedList<String> list = new LinkedList<String>();
//
//        //Adding elements to the linkedList
//        list.add("Steve");
//        list.add("Carl");
//        list.add("Tim");
//        System.out.println(list);
//
//        //adding an element to the first position
//        list.addFirst("Nyomi");
//        System.out.println(list);
//
//        // adding an element to the last position
//        list.addLast("Jim");
////        System.out.println(list);
//
//        //adding an element to third position using index
//        list.add(2, "Glenn");
////        System.out.println(list);
//
//        //using iterator interface
//        Iterator<String> iterator = list.iterator();
//        while (iterator.hasNext()){
//            System.out.println(iterator.next());
//        }
    }// end of main

    public static class stackExample{
        public ArrayList stack = new ArrayList();

        public stackExample(ArrayList stack){
            this.stack = stack;
        }
        public void push(Object obj){
            //adding object to stack
            stack.add(obj);
        }
        public Object pop() {
            //Returns and remove the top item from the stack
            //throws an EmptyStackException
            //if there are no elements in the stack
            if (stack.isEmpty()) // conditional if stack is empty
                throw new EmptyStackException(); // exception error
            return stack.remove(stack.size() - 1); // calling stack size and remove from index at -1
        }
            public boolean isEmpty(){
                //test whether the stack is empty
            return stack.isEmpty();
            }// end of class stack

    }
}// end of class

