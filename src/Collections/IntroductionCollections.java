package Collections;
//Collections - a group of objects joined together in a (collection) to represent a single entity.
// Within array there are limitations such as fixed size and arrays can only hold homogeneous (same kind) of data type
// Collections is a java framework comes from a standard library with the java.util package
// Used to support flexibility within a data structure to help with frequent programming tasks
 // In collections, size can be manipulated unlike arrays
// All elements in collections can be of the heterogeneous or homogeneous data type.
/* The collection framework is divided into four interfaces.
- List-linked list, vector, ArrayList, List
- Queue
- Set
- The three interfaces above inherit (extends) the collection interface
- Map-Hashmap, hashtable, sortedmap, treemap
 */
public class IntroductionCollections {

}
