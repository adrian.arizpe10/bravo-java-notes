import java.util.Scanner;

public class MethodsExercises {
    public static void main(String[] args) {
//        METHODS EXERCISE
//        Create a class named MethodsExercises. Inside of your class, write to code to create the specified methods.
//        Test your code by creating a main method and calling each of the methods you've created.
//        Basic Arithmetic
//        Create four separate methods. Each will perform an arithmetic operation:
//                  * Addition
//        System.out.println(Addition(4, 8));
////                * Subtraction
//        System.out.println(Subtraction(20, 5));
////                * Multiplication
//        System.out.println(Multiplication(4, 5));
////                * Division
//        System.out.println(Division(20, 1));
////        Each function needs to take two parameters/arguments so that the operation can be performed.
////                Test your functions and verify the output.
////        Add a modulus function that finds the modulus of two numbers.
//        System.out.println(Modulus(15, 5));
//                Food for thought: What happens if we try to divide by zero? What should happen?
//        Error
//                Create a method that validates that user input is in a certain range
//        The method signature should look like this:
//        System.out.print("Enter a number between 1 and 10: ");
//        int userInput = getInteger(1, 10);
//        Scanner scanner = new Scanner(System.in);
//        System.out.print("Enter a number between 1 and 10: ");
//        int userInput = getInteger(1, 10);
        getInteger(1, 10);

    } // End of Main
    public static int Addition(int a, int b){
        return a + b;
    }
    public static int Subtraction(int a, int b){
        return a - b;
    }
    public static int Multiplication(int a, int b){
        return a * b;
    }
    public static int Division(int a, int b){
        return a / b;
    }
    public static int Modulus(int a, int b){
        return a % b;
    }
    public static int getInteger(int min, int max){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter a valid number: ");
        int input = scanner.nextInt();
        if (input <= max && input >= min) {
            System.out.println("Thank you for following instructions!");
            return input;
        } else {
            System.out.println("Invalid Entry");
            return getInteger(min, max);
        }
    }


} // End of Class
