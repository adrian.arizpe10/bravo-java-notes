public class Arithmetic {
    // Static Property
    public static double pi = 3.14159;

    //Static Method
    public static int add(int x, int y){
        return x + y;
    }

    //Static method
    public static int multiply(int x, int y) {
        return x * y;
    }
}
