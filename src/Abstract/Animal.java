package Abstract;

public abstract class Animal {
    private double weight;
    public abstract void eat(String b);
    public abstract void getWeight(double weight);
}
