package Abstract;

public class Bird extends Animal {
    @Override
    public void eat(String b) {
        System.out.println("I am eat insects, and i am scared of cats");
    }

    @Override
    public void getWeight(double weight) {
        System.out.println("i am a bird who weighs: " + weight + " pounds");
    }
}
