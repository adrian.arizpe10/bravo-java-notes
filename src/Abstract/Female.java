package Abstract;

public class Female extends People {
    @Override
    public void sleep(String b) {
        System.out.println("I am sleeping");
        System.out.println(b);
    }

    @Override
    public void eat(String c) {
        System.out.println("I am eating");
    }

    @Override
    public void laugh(String d) {
        System.out.println("I am laughing...");
    }
}
