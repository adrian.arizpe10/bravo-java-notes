package Abstract;

public abstract class People {
    public abstract void sleep(String b );
    public abstract void eat(String c);
    public abstract void laugh(String d);

}
