package Abstract;

public abstract class Aliens {
    public abstract void driveSpaceships(String b);
    public abstract void age(double age);
}
