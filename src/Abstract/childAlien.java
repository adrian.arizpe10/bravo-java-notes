package Abstract;

public class childAlien extends Aliens {
    @Override
    public void driveSpaceships(String d) {
        System.out.println(d);
    }

    @Override
    public void age(double age) {
        System.out.println("I am: " + age + " years old");
    }
}
