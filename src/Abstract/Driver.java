package Abstract;

public class Driver {
    public static void main(String[] args) {
        Cat cat1 = new Cat();
        Animal cat2 = new Cat();
        Pet cat3 = new Pet();
        cat3.eat("");
        Animal bird1 = new Pet();
        bird1.getWeight( 2 );
        Bird tweety = new Bird();
        tweety.getWeight(3);
        tweety.eat("I am a bird eating. ");
    }
}
