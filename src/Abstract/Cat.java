package Abstract;

public class Cat extends Animal {
    @Override
    public void eat(String b) {
        System.out.println("I eat birds");
    }

    @Override
    public void getWeight(double weight) {
        System.out.println("I am a cat who weighs" + 3 + " pounds.");
    }
}
