package Abstract;

public class Pet extends Animal {
    public void change(){
        System.out.println("I am a pet and a cat.");
    }

    @Override
    public void eat(String b) {
        System.out.println("i am eating");

    }

    @Override
    public void getWeight(double weight) {
        System.out.println("I am a pet and can wight differently based on what kind of pet I am.");
    }
}
