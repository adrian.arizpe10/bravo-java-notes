package CodeboundGymProject;

import java.util.LinkedList;

public class CodeboundClubApp {
    public static void main(String[] args) {
        String mem;
        MembershipManagement mm = new MembershipManagement();
        MemberFileHandler mh = new MemberFileHandler();
        LinkedList<Member> members = mh.readFile();
        int choice = mm.getChoice();
        while (choice != -1)
        {
            switch (choice)
            {
                case 1:
                    mem = mm.addMembers(members);
                    mh.appendFile(mem);
                    break;
                case 2:
                    mm.removeMember(members);
                    mh.overwriteFile(members);
                    break;
                case 3:
                    mm.printMemberInfo(members);
                    break;
                default:
                    System.out.print("\nYou have selected an invalid option.\n\n");
                    break;
            }
            choice = mm.getChoice();
        }
        System.out.println("\nGood Bye");
    }
}
