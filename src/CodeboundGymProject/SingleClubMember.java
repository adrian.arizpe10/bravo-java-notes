package CodeboundGymProject;

public class SingleClubMember extends Member {
    private int club;

    public SingleClubMember(char memberType, Integer memberID, String name, double fees, int club) {
        super(memberType, memberID, name, fees, club);
        this.club = club;
    }
    public int getClub() {
        return club;
    }

    public void setClub(int club) {
        this.club = club;
    }

    public String toString() {
        return "SingleClubMember{" +
                "memberType='" + getMemberType() + '\'' +
                ", memberId=" + getMemberID() +
                ", name='" + getName() + '\'' +
                ", fees=" + getFees() + '\'' +
                ",club=" + club +
                '}';
    }
} // End of class
