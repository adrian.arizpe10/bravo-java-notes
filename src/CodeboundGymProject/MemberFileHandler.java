package CodeboundGymProject;

import java.util.LinkedList;
import java.io.*;

public class MemberFileHandler {

    public LinkedList<Member> readFile(){
        LinkedList<Member> m = new LinkedList();
        String lineRead;
        String[] splitLine;
        Member mem;
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader("./CodeboundGymProject/members.csv"));
            lineRead = reader.readLine();
            while (lineRead != null){
                splitLine = lineRead.split(", ");
                if (splitLine[0].equals("S")) {
                    mem = new SingleClubMember('S', Integer.parseInt(splitLine[1]), splitLine[2], Double.parseDouble(splitLine[3]), Integer.parseInt(splitLine[4]));
                }else
                {
                    mem = new MultiClubMember('M', Integer.parseInt(splitLine[1]), splitLine[2], Double.parseDouble(splitLine[3]), Integer.parseInt(splitLine[4]));
                }
                m.add(mem);
                lineRead = reader.readLine();
            }
        }
        catch (IOException e){
            System.out.println(e.getMessage());
        }
        return m;
    }
    public void appendFile(String mem){
        try (BufferedWriter writer = new BufferedWriter(new FileWriter("./CodeBoundGymProject/members.csv", true))) {
            writer.write(mem);
            writer.newLine();
        }catch (Exception e){
            System.out.println(e.getMessage());
        }    }

    public void overwriteFile(LinkedList<Member> m){
        String s;
        try (BufferedWriter writer = new BufferedWriter(new FileWriter("./CodeBoundGymProject/members.temp", false))){
            for (int i=0; i< m.size(); i++)
            {
                s = m.get(i).toString();
                writer.write(s + "\n");
            }
        }catch (IOException e){
            System.out.println(e.getMessage());
        }
        File newFileName = new File("./CodeBoundGymProject/members.temp");
        File oldFileName = new File("./CodeBoundGymProject/members.csv");
        oldFileName.delete();
        newFileName.renameTo(oldFileName);
    }


}
