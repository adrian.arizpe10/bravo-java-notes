package CodeboundGymProject;

public class MultiClubMember extends Member {
    public int getMembershipPoints() {
        return membershipPoints;
    }

    public void setMembershipPoints(int membershipPoints) {
        this.membershipPoints = membershipPoints;
    }

    private int membershipPoints;
    public MultiClubMember(char memberType, Integer memberID, String name, double fees, int membershipPoints) {
        super(memberType, memberID, name, fees, membershipPoints);
    }
//    public String toString() {
//        return "SingleClubMember{" +
//                "memberType='" + getMemberType() + '\'' +
//                ", memberId=" + getMemberID() +
//                ", name='" + getName() + '\'' +
//                ", fees=" + getFees() + '\'' +
//                ",membership points=" + membershipPoints +
//                '}';
//    }
    @Override
    public String toString() {
        return super.toString() + "," + membershipPoints;
    }

}
