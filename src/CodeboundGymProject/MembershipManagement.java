package CodeboundGymProject;

import java.util.InputMismatchException;
import java.util.LinkedList;
import java.util.Scanner;

public class MembershipManagement {
    private final Scanner userInput = new Scanner(System.in);

    private int getIntInput(){
        int choice = 0;
        do {
            try {
                choice = userInput.nextInt();
                System.out.println(choice);
            }catch (InputMismatchException e){
                System.out.println("You did not enter a valid number.");
            }
        }while (choice == 0);
        return choice;
    }

    private void printClubOptions(){
        System.out.println("1) Club Alpha");
        System.out.println("2) Club Bravo");
        System.out.println("3) Club Charlie");
        System.out.println("4) Multi Clubs");
    }

    public int getChoice(){
        int local;
        System.out.println("WELCOME TO ALPHA FITNESS CENTER" +
                "\n=============================" +
                "\nPlease select an option (or Enter -1 to quit):" +
                "\n1) Add Member" +
                "\n2) Remove Member" +
                "\n3) Display Member Information" +
                "\n=============================");
        local = getIntInput();
        return local;
    }

    public String addMembers(LinkedList<Member> m) {
        String name;
        int club;
        String mem;
        double fees;
        int memberID;
        Member mbr;

        Calculator<Integer> cal;

        Scanner scanner = new Scanner(System.in);

        System.out.println("Please enter the member’s name: ");
        name = scanner.nextLine();

        printClubOptions();
        System.out.println("Enter a club ID: ");
        club = scanner.nextInt();

        while (club < 1 || club > 4){
            System.out.println("Please enter a valid Club ID (1-4)");
            club = getIntInput();
        }

        //Assign member ID
        if (m.size() > 0)
            memberID = m.getLast().getMemberID() + 1;
        else
            memberID = 1;

        if (club != 4)
        {
            cal = (n) -> {
                switch (n) {
                    case 1:
                        return 900;
                    case 2:
                        return 950;
                    case 3:
                        return 1000;
                    default:
                        return -1;
                }
            };
            //Add member to LinkedList
            fees = cal.calculateFees(club);
            mbr = new SingleClubMember('S', memberID, name, fees, club);
            m.add(mbr);
            mem = mbr.toString();
            System.out.println("Single member has been added!");
        } else {
            cal = (n) -> {
                if (n == 4){
                    return 1200;
                } else {
                    return -1;
                }
            };
            fees = cal.calculateFees(club);
            mbr = new MultiClubMember('M', memberID, name, fees, 100);
            m.add(mbr);
            mem = mbr.toString();
            System.out.println("Multi member has been added!");
        }
        return mem;
    } // End of addMember Method

    public void removeMember(LinkedList<Member> m){
        int memberID;
        System.out.println("Please enter a member ID you want to remove: ");
        memberID = getIntInput();
        for ( int i = 0; i < m.size(); i++) {
            if (m.get(i).getMemberID() == memberID) {
                m.remove(i);
                System.out.println("Member Removed");
                return;
            }
        }
        System.out.println("Member ID not found");
    }

    public void printMemberInfo(LinkedList<Member> m){
        int memberID;
        System.out.println("Enter Member ID to display information: ");
        memberID = getIntInput();
        for (int i = 0; i<m.size();i++)
        {
            if (m.get(i).getMemberID() == memberID)
            {
                String[] memberInfo = m.get(i).toString().split(", ");
                System.out.println("\n\nMember Type = " + memberInfo[0]);
                System.out.println("Member ID = " + memberInfo[1]);
                System.out.println("Member Name = " + memberInfo[2]);
                System.out.println("Membership Fees = " + memberInfo[3]);
                if (memberInfo[0].equals("S"))
                {
                    System.out.println("Club ID = " + memberInfo[4]);
                }else
                {
                    System.out.println("Membership Points = " + memberInfo[4]);
                }
                return;
            }
        }
    }
} // End of Class
