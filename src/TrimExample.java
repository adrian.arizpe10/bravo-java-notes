import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class TrimExample {
    public static void main(String[] args) {
        List <Integer> arrayList = new ArrayList<Integer>();
        arrayList.add(1);
        arrayList.add(2);
        arrayList.add(3);
        arrayList.add(4);
        arrayList.add(5);
        arrayList.add(1);
        arrayList.add(1);
        arrayList.add(1);

        //Iterator has to be of the same datatype of arraylist
        Iterator <Integer> itr = arrayList.iterator(); // calling arrayList attaching iterator
        System.out.println(itr.next()); // print out next iteration by one only
        while (itr.hasNext()){//using this while we can get all the objects in the array
            Integer b = itr.next();//assigning integer variable attaching iterator
            System.out.println(b);//printing out array list using iterators
        }

    }
}
