import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class ArraysExercise {
    //TODO: Create an array that holds 3 Person objects. Assign a new instance of the Person class to each element.
// Iterate through the array and print out the name of each person in the array.
    //Then create a static method named addPerson. This method should accept an array of Person objects, as well as a single person object
    //to add to the passed array.
    //It should return an array whose length is 1 greater than the passed array, with the passed person object at the end of the array.
    //TODO: Take 15 integer user inputs and print them in the following:
    // number of positive numbers
    // number of negative numbers
    // number of odd numbers
    // number of even numbers
    // number of 0s
    public static void main(String[] args) {

        // Vowels in a string
//        String s;
//        Scanner sc = new Scanner(System.in);
//        System.out.println("Enter in a String: ");
//        s = sc.next();
//        System.out.println("Vowels in a String are " + s + " are:");
//        vowels(s);
//        integers();
//        smallest();
//        largest();
//        Person();
        math();

//        //TODO: Create an Array that will take the vowels of the alphabet and display them.
//        String[] alphabet = {"a", "e", "i", "o", "u"};
//        System.out.println(Arrays.toString(alphabet));
//        //TODO: Take 5 integer inputs from a user and store them in an array. SOUT out the integers *Hint:Scanner
//        Scanner scanner = new Scanner(System.in);
//        int [] intArray = new int[5];
//        System.out.println("Enter five integers");
//        for (int i = 0; i < 5; i++){
//            int input = scanner.nextInt();
//            intArray[i] = input;
//            System.out.println(intArray[i]);
//        }
        //TODO: Find the SMALLEST number in an array.
//        int [] Array =  { 10, 5, 6, 7, 8 };
//        int temp;
//        for (int i = 0; i < Array.length; i ++){
//            for (int a = i + 1; a < Array.length; a++){
//                if (Array[a] < Array[i]) {
//                    temp = Array[i];
//                    Array[i] = Array[a];
//                    Array[a] = temp;
//                }
//                System.out.println(Array[0]);
//            }
//        }

    } // end of main


    //TODO: Create an Array that will take the vowels of the alphabet and display them.
    static void vowels(String str){
        char ch;
        int i = 0;
        for (int j = 0; j < str.length(); j++){
            ch = str.charAt(j);
            if (ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u' || ch == 'A' || ch == 'E' || ch == 'I' || ch == 'O' || ch == 'U'){
                i = 1;
                System.out.println(ch);
            }
        }
        if (i == 0){
            System.out.println("There are no vowels entered in a string");
        }
    }
    //TODO: Take 5 integer inputs from a user and store them in an array. SOUT out the integers *Hint:Scanner
    public static void integers() {
        Scanner sc = new Scanner(System.in);
        int[] nums = new int[5];

        for (int i = 0; i < nums.length; i += 1) {
            System.out.println("Create your array: [" + i + "]");
            nums[i] = sc.nextInt();
        }
        for (int i = 0; i < nums.length; i += 1) {
            System.out.println(nums[i]);
        }
    }
    //TODO: Find the SMALLEST number in an array.
public static void smallest(){ // creating a method called smallest
        Scanner scanner = new Scanner(System.in); // creating a scanner for user input
        int [] nums = new int[5]; //declaring int array creating int object and assigning 5 values

        for (int i = 0; i < nums.length; i +=1){ // Looping through the array
            System.out.println("Please enter your numbers for the array: [" + i + "]"); // grabbing index of array
            nums[i] = scanner.nextInt(); // setting index of array to scanner nextInt
        }
        int smallest = nums[0];//creating int variable assigning nums array indexed at 0

        for (int i = 0; i < nums.length; i += 1){ // creating a for loop to loop through indexed array
            if (nums[i] < smallest) // conditional to compare array to smallest
                smallest = nums[i]; // smallest variable assigning nums array indexing
        }
    System.out.println("Smallest number is: " + smallest); // printing out smallest index
}
//TODO find largest number in an array

    public static void largest() {
        Scanner bc = new Scanner(System.in);
        int[] nums = new int[5];
        for (int i = 0; i < nums.length; i +=1){
        System.out.println("Please enter your numbers for the array " + i + ".");
        nums[i] = bc.nextInt();
    }
        int largest = nums[0];

        for (int i = 0; i < nums.length; i += 1) {
            if (nums[i] > largest)
                largest = nums[i];
        }
        System.out.println("Largest number is " + largest);
    }
    //TODO: Create an array that holds 3 Person objects. Assign a new instance of the Person class to each element.
//public static void Person(){
//        String person[] = new String[3];
//        person[0] = "Jim";
//        person[1] = "John";
//        person[2] = "Jill";
//        for (int i = 0; i < person.length; i ++){
//            System.out.println(person[i]);
//
//            Person p1 = new Person("Jane");
//            Person p2 = new Person("Joe");
//            Person p3 = new Person("Jack");
//
//            Person[] people = {p1, p2, p3};
//            people[0] = p1;
//            people[1] = p2;
//            people[2] = p3;
//            for (int i = 0; i < people.length;i++){
//                System.out.println(people[i].addPerson);
//            }
//            public static Person[] addPerson(){
//                Person[] per
//            }
//        }
//}
    //TODO: Take 15 integer user inputs and print them in the following:
    // number of positive numbers
    // number of negative numbers
    // number of odd numbers
    // number of even numbers
    // number of 0s
    public static void math(){
        Scanner scd = new Scanner(System.in);
        int [] inputArr = new int[15];
        int pos = 0;
        int neg = 0;
        int odd = 0;
        int even = 0;
        int zero = 0;
        for (int i = 0; i < inputArr.length; i +=1){
            System.out.println("Print out the numbers in the array: " + i);
            inputArr[i] = scd.nextInt();
            if (inputArr[i] > 0) {
                pos++;
            } else if (inputArr[i] < 0) {
                neg++;
            }else {
                zero++;
            }
            if (inputArr[i] % 2 == 0) {
                even++;
            } else {
                odd++;
            }
        }
        System.out.println("Positive numbers are: " + pos + " \nThe negative numbers are: " + neg + "\n The odd numbers are: " + odd + "\n The even numbers are: " + even + "\n The zero's are: " + zero
        );
    }



} // end of class
