public class StringsLesson {
    public static void main(String[] args) {
        // Strings
//        String message = "Hello World";
//        System.out.println(message);
//        String anotherMessage;
//
//        anotherMessage = "Another message assigned!";
//        System.out.println(anotherMessage);

        //Concatenation
//        String myName = "Hello World, " + "my name is Stephen";
//        System.out.println(myName);
//        System.out.println(message + " " + anotherMessage + " " + myName);
//        if (message = "Hello") // do not do this

//        String message = "Hello World";
//        if (message.equals("Hello World")) {
//                System.out.println("Message is Correct");
//            }

//        String Comparison Methods
//        .equals(), .equalsIgnoreCase(), .startsWith(), .endsWith()

        String input = "Bravo Rocks!";
        System.out.println(input.equals("Bravo Rocks!")); // true
        System.out.println(input.equals("bravo rocks!")); // false

        System.out.println(input.equalsIgnoreCase("BRAVO ROCKS!")); // true
        System.out.println(input.equalsIgnoreCase("BRAVO ROCKS")); // false

        System.out.println(input.startsWith("Bravo")); // true
        System.out.println(input.startsWith("bravo")); // false

        System.out.println(input.endsWith("s")); // false
        System.out.println(input.endsWith("!")); // true

        System.out.println(input.endsWith("Rocks!")); // true
        System.out.println(input.endsWith("rocks!")); // false




    } // end of main
} // end of class
