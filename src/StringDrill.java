public class StringDrill {
    public static void main(String[] args) {
//        STRING DRILL
//        String Basics. Create a class named StringDrill with a main method.
//    * For each of the following output examples, create a string variable named message that contains the desired output and print it out to the console.
//                * Do this with only one string variable and one print statement.
        String message = "I never wanna hear you say";
        String message2 = "I want it that way!";
        String message3 = "Check \"this\" out!, \"s inside of \"s!\"";
        String message4 = "In windows, the main drive is usually C:\\\\";
        String message5 = "I can do backslashes \\, double backslashes \\\\, and the amazing triple backslash \\\\\\!";
        System.out.println(message);
        System.out.println(message2);
        System.out.println(message3);
        System.out.println(message4);
        System.out.println(message5);
//
    }
}
