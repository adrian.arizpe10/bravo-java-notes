package ExceptionHandling;

public class ExceptionHandling {
    /*
    Exception handling or error handling
    - allows us to control the flow of a program when an error occurs
    try-catch-finally statement
     */
    // Syntax for a try-catch-finally statement
    /*try{
        code to do something
    }
    catch (type of error){
    do something else when an error occurs
    }
    finally {
    code that will run regardless of the try or catch condition is met
    }
     */
}
