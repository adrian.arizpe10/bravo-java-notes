import java.text.DecimalFormat;
import java.util.Scanner;

public class ConsoleExercise {
    public static void main(String[] args) {

//        For the following exercises, create a new class named ConsoleExercise with a main method like the one in your HelloWorld class.
//        Copy this code into your main method:
        double pi = 3.14159;
        DecimalFormat dc = new DecimalFormat("#.00");
//        System.out.format(dc.format(pi));
//        System.out.format("The value of pi is approximately %.2f", pi);
//        Write some java code that uses the variable pi to output the following:

        //        The value of pi is approximately 3.14.
//                Don't change the value of the variable, instead, reference one of the links above and use System.out.format to accomplish this.
//        Explore the Scanner Class
//        Prompt a user to enter a integer and store that value in an int variable using the nextInt method.
        Scanner scanner = new Scanner(System.in);
//        System.out.print("Please enter an integer: ");
//        int num = scanner.nextInt();
//        System.out.println("You entered: " + num);
//        What happens if you input something that is not an integer?
//        Prompt a user to enter 3 words and store each of them in a separate variable, then display them back, each on a newline.
//        String string1;
//        String string2;
//        String string3;
//        System.out.println("Enter 3 words: ");
//        string1 = scanner.nextLine(); // nextLine prints out all the words
//        string2 = scanner.nextLine();
//        string3 = scanner.nextLine();
//        System.out.println("You entered: " + string1 + " " + string2 + " " + string3 + ".");
//        String word1 = scanner.next();
//                What happens if you enter less than 3 words?
//                What happens if you enter more than 3 words?
//                Prompt a user to enter a sentence, then store that sentence in a String variable using the .next method, then display that sentence back to the user.
//        System.out.println("Enter a sentence: ");
//        String userInput = scanner.nextLine();
//        System.out.println(userInput);
//        do you capture all of the words?
//        Rewrite the above example using the .nextLine method.
//        Calculate the perimeter and area of Code Bound's classrooms
//        Prompt the user to enter values of length and width of a classroom at Code Bound.
        System.out.println("Enter the length and width: ");
        String num1 = scanner.nextLine();
        String num2 = scanner.nextLine();
        double number1 = Double.parseDouble(num1);
        double number2 = Double.parseDouble(num2);
        System.out.println(number1 + number2);
        System.out.println((2 * number1 + 2 * number2));
        double area = number1 * number2;
        double perimeter = number1 * 2 + number2 * 2;
        System.out.println(area);
        System.out.println(perimeter);

//        Use the .nextLine method to get user input and cast the resulting string to a numeric type.
//•	Assume that the rooms are perfect rectangles.
//•	Assume that the user will enter valid numeric data for length and width.
//                Display the area and perimeter of that classroom.
//                The area of a rectangle is equal to the length times the width, and the perimeter of a rectangle is equal to 2 times the length plus 2 times the width.
//                Bonuses
//•	Accept decimal entries
//•	Calculate the volume of the rooms in addition to the area and perimeter
    }
}
