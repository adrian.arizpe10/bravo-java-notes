package Inherit;

public class Persons { // creating a class
    String name;

    public Persons(String name) {
        this.name = name;
    }

    public void sayHello(){ // sayHello method
        System.out.println("Hello from:" + name); // printing out a string
    }
}
abstract class Employee{
    protected String name;
    protected String department;

    public Employee(String name, String department) {
        this.name = name;
        this.department = department;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }
    public abstract String work();
}
//class Developer extends Employee{
//    public String work(){
//        return "Writing code";
//    }
//}
//class Manager extends Employee{
//    @Override
//    public String work() {
//        return "working";
//    }
//
//    public abstract class work(){
//        return;
//    }
//}
//class Employee extends Persons {
//    private double salary;
//
//    public Employee(String name) {
//        super(name);
//    }
//
//    public void doWork(){
//        System.out.println("Get to work!!!");
//    }
//    public void sayHello(){ // overwriting method
//        System.out.println("I say Hello differently!");
//    }
//    public String getName() {
//        return name;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }
//}
//class SuperHero extends Persons {
//    private String alterEgo;
//    public SuperHero(String name, String alterEgo){
//        super(name);
//        this.alterEgo = alterEgo;
//    }
//}

