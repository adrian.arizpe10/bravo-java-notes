package Arrays;

import java.util.Arrays;

public class arrays {
    //Arrays
    //list of data that contains zero or more items called elements
    //Arrays can only be of one data type within a single array

    //Syntax for array
    //Declaring an array of data type double - assigning a variable of prices
    double [] prices;
    int [] integers;
    String [] names;
    float [] floatingNumbers;

    public static void main(String[] args) {
//        String [] names = new String[6];
//        names[0] = "Adrian";
//        names[1] = "Sandra";
//        names[2] = "MaryAnn";
//        names[3] = "Henry";
//        names[4] = "Jonathan";
//        names[5] = "Eric";
//        System.out.println(names[0]);
//        System.out.println(names[3]);

//        String[] avengers = {"Captain America", "Iron Man", "Hulk", "HawkEye", "Thor", "Black Widow"};
//        System.out.println(avengers.length);

        //iterating over arrays
        int[] numbers = new int[5];
        numbers[0] = 1;
        numbers[1] = 2;
        numbers[2] = 3;
        numbers[3] = 4;
        numbers[4] = 5;
        for (int i = 0; i <numbers.length; i++) {
            System.out.println(numbers[i]);
        }
        String[] languages = {"html", "css", "javascript", "angular", "java"};
//        for (int i = 0; i < languages.length; i++){
//            System.out.println(languages[i]);
//        }
        //Enhanced for loop wit the same result of printing array
        for (String language: languages){
            System.out.println(language);
        }
        //Example iterating through a 2d array
        int [] [] matrix = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        };
        for (int i = 0; i < matrix.length; i++){
            System.out.println(Arrays.toString(matrix[i]));
        }
    }

}
