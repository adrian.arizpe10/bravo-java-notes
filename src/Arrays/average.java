package Arrays;

import java.util.Scanner;

public class average {
    //Creating scanner to receive input from user
    public static Scanner scanner = new Scanner(System.in);
// main method where we call our methods
    public static void main(String[] args) {
        //declaring an int array declaring a variable myIntArray and assigning getIntegers method putting value in parameters
        int [] myIntArray = getIntegers(5);
        //print array called myIntArray
        printArray(myIntArray);
        //printing out the average by calling getAverage method
        System.out.println("Average is: " + getAverage(myIntArray));
    } // end of main
// creating a method to getIntegers takes in a number prints out integers in the array
    public static int[] getIntegers(int number){
        System.out.println("Please enter " + number + " integer values ");
        int intArray[] = new int [number]; // declaring an array creating array object taking in int number thats parameters in method
        for (int i = 0; i<number;i++){ // looping through numbers
            intArray[i] = scanner.nextInt(); // assigning int array scanner to the nextInt
        }
        return intArray; // returning array
    }
    public static void printArray(int[] intArray){
        for (int i = 0; i < intArray.length; i++){ // looping through array
            System.out.println(intArray[i]); // printing out items indexed in array
        }
    }
    public static double getAverage(int[] intArray){ // method to get average
        int sum = 0; // creating sum object and assigning value of 0
        for (int i = 0; i < intArray.length; i++){ // looping through int array
            sum += intArray[i]; // now assigning sum object to intArray
        }
        return(double) sum / (double) intArray.length; // returning sum dividing it by length of array
    }
} // end of class
