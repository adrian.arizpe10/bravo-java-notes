package Arrays;

public class multidimensionalArray {

    public static void main(String[] args) {
        // Create a 2d array
        int[][] a = {
                {1, 2, 3},
                {4, 5, 6, 9},
                {7},
                {10}
        };
        // Calculate length of each row
        System.out.println("First element first row " + a[0][0]);
        System.out.println("Third element in second row " + a[1][2]);
        System.out.println(a[2][0]);
        System.out.println(a[3][0]);
    }
}
