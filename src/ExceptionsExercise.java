import java.util.InputMismatchException;
import java.util.Scanner;

public class ExceptionsExercise {
//    Refactor your grading system
//    Remember from our ControlFlowExercise our Grading System?
//    Copy your code for your Grading System and paste it into your ExceptionsExercise class.
//    Refactor your code to include it in a try-catch-finally where if the user did not enter a number, it "catches" the error and notifies user that he/she "did not enter a number". But this doesn't end the program, after it catches the user's error, have the program prompt the user "Would you like to continue?", and based on the user's answer, continue the program or end the program.
//    Use the following code: (you may need to comment out your code from the first task to avoid errors)
//    int choice = 0;
//    Scanner input = new Scanner(System.in);
//    int[] numbers = { 10, 11, 12, 13, 14, 15 };
//        System.out.print("Please enter the index of the array: ");
//        try {
//        choice = input.nextInt();
//        System.out.printf("numbers[%d] = %d%n", choice, numbers[choice]);
//    }
//    Include three catch blocks:
//    First one, if the user tries to access an element of an array with an index that's outside its bounds.
//    Second,  if the user's input does not match the expected type (in this case, an integer)
//    Last, a general exception for a general error.
public static void main(String[] args) {
//    Scanner scan = new Scanner(System.in);
//    System.out.println("Do you want to continue? y/n");
//    String userInput = scan.nextLine();
//    do {
//            try{
//                Scanner scanner = new Scanner(System.in);
//                System.out.println("Enter a grade number: ");
//                int grade = scanner.nextInt();
//                if (grade >= 90 && grade <= 100) {
//                    System.out.println("You got an A");
//                } else if (grade >= 80 && grade < 90) {
//                    System.out.println("You got a B");
//                } else if (grade >= 70 && grade < 80) {
//                    System.out.println("You got a C");
//                } else if (grade >= 60 && grade <= 66) {
//                    System.out.println("You got a D");
//                } else if (grade >= 0 && grade < 60) {
//                    System.out.println("You got an F");
//                } else {
//                    System.out.println("You did not enter a valid grade number 0-100");
//                }
//            } catch (Exception e){
//                System.out.println(e.getMessage());
//                System.out.println("You did not enter a number between 0-100");
//            }
//            finally {
//                System.out.println("Do you want to continue? y/n");
//                userInput = scan.nextLine();
//            }
//        } while (userInput.equals("y"));
    int choice = 0;
    Scanner input = new Scanner(System.in);
    int[] numbers = { 10, 11, 12, 13, 14, 15 };
    System.out.print("Please enter the index of the array: ");
    try {
        choice = input.nextInt();
        System.out.printf("numbers[%d] = %d%n", choice, numbers[choice]);
    }
    catch (ArrayIndexOutOfBoundsException e){
        System.out.println(e.getMessage()); // default message
        System.out.println("Error: Index is invalid");
    }
    catch (InputMismatchException e){
        System.out.println(e.getMessage());
        System.out.println("Error: You did not enter an integer");
    }
    catch (Exception e){
        System.out.println(e.getMessage());
    }

} // End of Main
} // End of Class

