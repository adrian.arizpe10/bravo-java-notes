import java.util.Scanner;

public class ControlFlowExercises {
    public static void main(String[] args) {
//        EXERCISE 4 CONTROL FLOW STATEMENTS
//        For all of the following exercises, create a new class named ControlFlowExercises with a main method. After each exercise, commit your changes, then replace your code in the main method with the next exercise.
//                Loop Basics
//        While
//        Create an integer variable i with a value of 9.
//        Create a while loop that runs so long as i is less than or equal to 23
//        Each loop iteration, output the current value of i, then increment i by one.
//        Your output should look like this:
//        9 10 11 12 13 14 15 16 17 18 19 20 21 22 23
        int i = 9;
        while (i <= 23) {
            System.out.println(i);
            i++;
        }
//        Do While
//        Create a do-while loop that will count by 2's starting with 0 and ending at 100. Follow each number with a new line.
        int num = 0;
        do {
            System.out.println(num + "\n");
            num += 2;
        } while (num <= 100);

//        Alter your loop to count backwards by 5's from 100 to -10.
        int hundred = 100;
        do {
            System.out.println(hundred);
            hundred -= 5;
        } while (hundred >= -10);

//        Create a do-while loop that starts at 2, and displays the number squared on each line while the number is less than 1,000,000. Output should equal:
        long number = 2;
        do {
            System.out.println(number);
            number *= number;
        } while (number <= 1000000);

        int numba = 2;
        do {
            System.out.println(numba);
            numba = (int)Math.pow(numba, 2);
        } while (numba <= 1000000);
//        2
//        4
//        16
//        256
//        65536
//        For Loop
//        Refactor the previous two exercises to use a for loop instead
//        for (int i = 100; i >= -10; i -= 5) {
//            System.out.println(i);
//        }
//        for (long num = 2; num <= 1000000; num *= num) {
//            System.out.println(num);
//        }
//        Fizzbuzz
//        One of the most common interview questions for entry-level programmers is the FizzBuzz test. Developed by Imran Ghory, the test is designed to test basic looping and conditional logic skills.
//                Write a program that prints the numbers from 1 to 100.
//        For multiples of three print "Fizz" instead of the number
//        For the multiples of five print "Buzz".
//                For numbers which are multiples of both three and five print "FizzBuzz".
//        for (int i = 0; i <= 100; i++) {
//            if (i % 3 == 0 && i % 15 != 0) {
//                System.out.println("Fizz");
//            } else if (i % 5 == 0 && i % 15 != 0) {
//                System.out.println("Buzz");
//            } else if (i % 15 == 0) {
//                System.out.println("FizzBuzz");
//            } else {
//                System.out.println(i);
//            }
//        }
//                Display a table of powers.
//        Prompt the user to enter an integer.
//                Display a table of squares and cubes from 1 to the value entered.
//        Ask if the user wants to continue.
//        Assume that the user will enter valid data.
//        Only continue if the user agrees to.
//        Example Output
//        What number would you like to go up to? 5
//        Here is your table!
//                number | squared | cubed
//                ------ | ------- | -----
//                1      | 1       | 1
//        2      | 4       | 8
//        3      | 9       | 27
//        4      | 16      | 64
//        5      | 25      | 125

//        System.out.println("Welcome to the table of powers");
//        Scanner scanner = new Scanner(System.in);
//        System.out.println("Would you like to continue?");
//        String userInput = scanner.next();
//        boolean confirm = userInput.equals("y");
//        while (confirm == true) {
//            System.out.println("Please enter a number to go up to in our table of powers: ");
//            int num = scanner.nextInt();
//            System.out.println("Number" + "  " + "Squared" + "  " + "Cubed");
//            System.out.println("-----" + "  " + "-----" + "  " + "-----");
//            for (int i = 1; i <= num; i++) {
//                System.out.println(i + "\t\t" + (i * i) + "\t\t" + (i * i * i));
//            }
//        }
//        Convert given number grades into letter grades.
//                Prompt the user for a numerical grade from 0 to 100.
//        Display the corresponding letter grade.
//                Prompt the user to continue.
//        Assume that the user will enter valid integers for the grades.
//        The application should only continue if the user agrees to.
//        Grade Ranges:
//        A : 100 - 90
//        B : 89 - 80
//        C : 79 - 70
//        D : 66 - 60
//        F : 59 - 0
//    Scanner scanning = new Scanner(System.in);
//        String input;
//        do {
//            System.out.println("Enter a grade number: ");
//            int grade = scanning.nextInt();
//            if (grade >= 90 && grade <= 100) {
//                System.out.println("You got an A");
//            } else if (grade >= 80 && grade < 90) {
//                System.out.println("You got a B");
//            } else if (grade >= 70 && grade < 80) {
//                System.out.println("You got a C");
//            } else if (grade >= 60 && grade <= 66) {
//                System.out.println("You got a D");
//            } else if (grade >= 0 && grade < 60) {
//                System.out.println("You got an F");
//            } else {
//                System.out.println("You did not enter a valid grade number 0-100");
//            }
//            System.out.println("Would you like to continue?");
//            input = scanning.next();
//        } while (input.equalsIgnoreCase("yes"));
//        System.out.println("Have a nice day!");
//        Bonus
//        Edit your grade ranges to include pluses and minuses (ex: 98-100 = A+, 94-97 = A, 90-93 = A-).
    }
}
