public class Human {
    private String firstName;
    private String lastName;

    public Human() {
    }
    public String getName(){
//TODO: return the person's name
        return String.format("%s %s", firstName, lastName);
    }
    public void setName(String firstName, String lastName){
//TODO: change the name property to the passed value
        this.firstName = "Billy";
        this.lastName = "Bob";
    }
    public void sayHello(){
//TODO: print a message to the console using the person's name
        System.out.println(firstName);
    }
//    The class should have a constructor that accepts a `String` value and sets
//    the person's name to the passed string.
//    Create a `main` method on the class that creates a new `Person` object and
//    tests the above methods.
public static void main(String[] args) {
    Human firstPerson = new Human();
    firstPerson.firstName = "Johnny";
    firstPerson.lastName = "Bravo";
    System.out.println(firstPerson.getName());
//    System.out.println(firstPerson.setName());
}
}
