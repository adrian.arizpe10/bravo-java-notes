import java.util.Scanner;

public class ControlFlowLesson {
    public static void main(String[] args) {
        //Comparison operators
//        System.out.println( 5 != 2); // true
//        System.out.println(5 >= 5); // true
//        System.out.println(5 <= 5); // true

        // != , <, >, <=, >=, ==

        //Logical Operators ||, &&
//        System.out.println(5 == 6 && 2 > 1 && 3 != 3); // false
//        System.out.println(5 != 6 && 2 > 1 && 3 == 3); // true
//        System.out.println(5 == 5 || 3 != 3); // true || false = true
    //If Statement
        /*
        syntax:
        if (condition 1 is met){
        do task 1
        } else if (condition 2 is met){
       do task 2
       } else {
       do task 3
       }
         */
//        int score = 34;
//        if (score >= 50) {
//            System.out.println("New High Score!");
//        } else {
//            System.out.println("Try Again!");
//        }
//        //String Comparison
//        Scanner sc = new Scanner(System.in);
//        System.out.print("Would you like to continue? [y/N]");
//        String userInput = sc.next();

        //boolean confirm = userInput == "y"; //DO NOT DO THIS

        //DO THIS
//        boolean confirm = userInput.equals("y");
//        if (confirm == true) {
//            System.out.println("Welcome to the jungle");
//        } else if (userInput.equals("n")) {
//            System.out.println("Welcome to the jungle...you enter a lowercase n");
//        } else {
//            System.out.println("Bye Bye Bye");
//        }
//
        //DO THIS!
//        if (userInput.equals("y")) {
//            System.out.println("Welcome to the jungle");
//        }

        //DONT DO THIS IN JAVA
//        if (userInput == "y") {
//            System.out.println("Welcome to the jungle");
//        }

        //Switch
        //Syntax
        //switch (variable used for "switching") {
//        case firstCase: do task A;
//        break;
//        case secondCase: do task B;
//        break;
//        default: do task C;

//        Scanner input = new Scanner(System.in);
//        System.out.println("Enter your grade: ");
//        String userGrade = input.nextLine().toUpperCase();
//        switch (userGrade) {
//            case "A":
//                System.out.println("Distinction");
//                break;
//            case "B":
//                System.out.println("B Grade");
//                break;
//            case "C":
//                System.out.println("C Grade");
//                break;
//            case "D":
//                System.out.println("D Grade");
//                break;
//            default:
//                System.out.println("Fail");
//        }
        //while
        /*
        SYNTAX:
        while (conditional) {
        // loop
         */
//        int i = 0;
//        while (i < 10) {
//            System.out.println("I is " + i);
//            i++;
//        }

        // Do while Loop
        /*
        do {
        //Statement
        } while (condition is true)
         */
//        do {
//            System.out.println("You will see this!");
//        } while (false);

        //FOR LOOP
//        for (var i = 0; i <= 10; i += 1) {
//            System.out.println(i);
//        }

        //ESCAPE SEQUENCES - dealing with strings
        System.out.println("Hello \nWorld"); // \n Acts like a new line - <br>
        System.out.println("Hello \tWorld"); // \t Acts like a tab
    } // end of main
} // end of class
