import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

public class CollectionsHashMapExercise {
    public static void main(String[] args) {
        HashMap <String, String> map = new HashMap<>();
        map.put("Henry", "Student");
        map.put("Jonathan", "Student");
        map.put("MaryAnn", "Student");
        map.put("Eric", "Student");
        map.put("Adrian", "Student");
        map.put("Sandra", "Student");

        map.replace("Adrian", "Krispy");

        System.out.println(map);

        for (String i : map.keySet()){
            System.out.println("Key: " + i + "Value: " + map.get(i));
        }

        Iterator<String> it = map.keySet().iterator();
            while (it.hasNext()) {
                String key = it.next();
                System.out.println(map);
            }

        System.out.println(map.isEmpty());

        HashMap <Integer, String> map2 = new HashMap<>();
        map2.put(1, "Adrian");
        map2.put(2, "Henry");
        map2.put(3, "MaryAnn");
        map2.put(4, "Sandra");
        map2.put(5, "Jonathan");
        map2.put(6, "Eric");

        System.out.println(map2.get(2));

//        Create a program to test if a map contains a mapping for the specified key.
        if (map2.containsKey(3)){
            System.out.println("Map2 Contains Key 3, this hold the value of: " + map2.get(3));
        }
//                Create a program to test if a map contains a mapping for the specified value.
        if (map2.containsValue("Henry")){
            System.out.println("Map2 contains Value \"Henry\".");
        }

//                Create a program to get a set view of the keys in a map

        Set keySet = map2.keySet();
        System.out.println("Key set values are " + keySet);
//        Create a program to get a collection view of the VALUES contained in a map.

        System.out.println("Collection view is: " + map2.values());

    } // End of Main
//    public void checkForKey(){
//        if
//    }
    } // End of class

