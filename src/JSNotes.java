public class JSNotes {
    /**Object - an instance of a class
     * -has properties and methods
     * -Instantiated with the new keyword
     *
     * Class - template or blue-print that we use for objects
     * Field - a variable or method that belongs to a class
     */
//    Objectives within javaII
//    Create and use instances of classes
//    Defines custom classes that extend other classes and implement interfaces
//    Create interfaces and abstract classes
//    Use collections to work with groups of data
//    //object oriented programming
//    Some of what I caught
//    Key terms:
//    Object - software bundle of related state and behavior
//    Class - a blueprint or template from which objects are created based on class
//    Inheritance - a process of defining a new class based on an existing class by extending its common data member and methods
//            Interface -
//    Field- a variable or method that belongs to an object or class
//    Objects:
//    State: (Fields/Variable) Name, Color, Breed
//    Behavior : Barking, Wagging Tail, Hungry
//    Syntax of Object:
//    Classname objectname; 		<—Declaration of object
//            Objectname = new classname(); 	<— allocate member to object .(being object
//    Shorthand syntax:
//    Classname objectname = new classname();
//    Objectives within java II
//-Create and use instances of classes
//-Defines custom classes that extend other classes and implement interfaces
//-Create interfaces and abstract classes
//-Use collections to work with groups of data
//-Use Java to read and write to files
//    //object oriented programming (OOP)
//    Key terms:
//    Object - software bundle of related state and behavior (instance of a class)
//    Class - a blueprint or template to stick objects in
//    Inheritance - a process of defining a new class based on an existing class by extending its common data member and methods. Provide the ability to reuse of code, it improves reusability in your java application
//    Interface - special case of an abstract class, use to define the behavior of objects and the way it interacts with the rest of the application
//    Field- a variable or method that belongs to an object or class
//    Package- a namespace for organization
//    Objects:
//    State: (Fields/Variable) Name, Color, Breed
//    Behavior : (Methods/Functions) Barking, Wagging Tail, Hungry
//    Syntax of Object:
//    Classname objectname; 		<—Declaration of object
//            Objectname = new classname(); 	<— allocate memory to object.(define object)
//    Shorthand syntax: To directly define an object
//    Classname objectname = new classname();
//    BREAKING DOWN THE CLASS SYNTAX:
//            |*class name
//		access modifier->			public class Vehicle {.
//        private int doors;     			<————|
//        Class members Instance Variables->	private int speed;						|
//        private String color;					|
//        Public void run() {				<————| Class Body
//
//            //Run method implementation
//        }
//    }
//    Static in Java:
//            -Static variables area also know as class variables, default values based on data types
//-Memory allocation for such variables only happens once when the class is loaded in the memory
//            Rules of keyword Static:
//    The static variable can be used to refer to the common property of all object (not unique properties ) ex: college name for students
//    Constructors: - special method called when an object is created
//    Properties:
//    Constructor name must be the same as class name
//Will be called automatically
//        Can’t return any value including void
//    Should be static
//    Cannot inherit
//    Without parameters
//    JVM: Java Virtual Memory
}
