import java.util.Scanner;

public class HelloWorld {
    public static void main(String[] args) {
//        System.out.print("Hello World");
//        System.out.print("code inside of the curly braces");

        //Data Types
        //8 Primitive Data Types
        //byte - a very short int
//        byte age = 12;
//        short myShort = -32768;
//        int myInt = 120;
//        long myLong = 34565432365L;
//        float myFloat = 123.342F;
//        double myDouble = 123.342;
//        char myChar = 'B';
//        boolean myBoolean = true;
//
//        int x = 10;
//        int y = 25;
//        System.out.println(x + y);
//        System.out.println("x + y = " + (x + y));
//
//        byte myByte2 = 127;

        int favoriteNum = 7;
//        char myName = 3.14159;
//        float myNum = 3.14F;
//        long myNum;
//        System.out.println(myNum);
//        int x = 10;
//        System.out.println(x++);
//        System.out.println(x);
//        int x = 10;
//        System.out.println(++x);
//        System.out.println(x);
//
//        int class = 2;
//        String theNumberEight = "eight";
//        Object o = theNumberEight;
//        int eight = (int) o;
//        int eight = (int) "eight";
//        int x = 5;
//        x+=6;
//        int x = 7;
//        int y = 8;
//        x +=y;
//        int x = 20;
//        int y = 2;
//        x += y;
//        y -=y;
//        int z = Integer.MAX_VALUE + 1;
//        System.out.println(z);

        //Printing our Output
//        String name = "Bravo";
//        System.out.format("Hello there, %s. Nice to see you.", name);

//        String greet = "Hola";
//        System.out.format("%s, %s!", greet, name);
//        System.out.println(greet + ", " + name + "!");

        //Scanner Class - Get input from the console
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter Something: "); // Prompting the user to enter data
        String userInput = scanner.next(); // Obtaining the value the user input
        System.out.println(userInput); // souting the userInput value

    } // End of Main method
} // End of Class
